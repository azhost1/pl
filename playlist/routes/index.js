"use strict";

let playlist = require("../bin/playlist/index.js");

let Route;
(function (Route) {

	class Index {

		live(req, res, next) {
			//res.setHeader("Access-Control-Allow-Origin", "*");

			playlist.processRequest(req, res);
		}

		hp(req, res, next) {
			//res.setHeader("Access-Control-Allow-Origin", "*");

			playlist.health(req, res);
		}

		crossDomain(req, res, next) {
			//res.setHeader("Access-Control-Allow-Origin", "*");

			res.header('Content-Type','text/xml');
			res.send('<cross-domain-policy>' +
				'<allow-access-from domain="*" secure="false"></allow-access-from>' +
				'<allow-http-request-headers-from domain="*" headers="*" secure="false">' +
				'</allow-http-request-headers-from>' +
			'</cross-domain-policy>');
		}
	}

	Route.Index = Index;
})(Route || (Route = {}));

module.exports = Route;
