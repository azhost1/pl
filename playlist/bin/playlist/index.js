let settings = require('./config/index')(__dirname + '/config/app.json');
//let Variables = require('./variables')(settings.get("variablesEndpoint"));
//let crypto = require('crypto');


let User = require('../user')(8);
let PlaylistError = require('./Error')();


let ENV = settings.get("ENV");
let LivePlaylist = require('./live')(settings.get("CDN")[ENV]);
let VodPlaylist = require('./vod')(settings.get("CDN")[ENV]);


let playlist = module.exports = {};

/**
 * @todo: for test
 */
setInterval(function(){
	settings = require('./config/index')(__dirname + '/config/app.json');
}, 300000);


playlist.health = function(req, res){
	let health = LivePlaylist.health();

	if(!health) return sendResponse(res, {
		health: "KO",
		ERROR: "Loading data..."
	}, true);

	return sendResponse(res, health);
};



/**
 *
 * @param req
 * @param res
 */
playlist.processRequest = function(req, res){

	if(typeof req.body.data !== "string") return sendResponse(res, PlaylistError.get("NOT_VALID_REQUEST"));

	let user, request;

	try {
		user = User.getUser(req);
		request = JSON.parse(req.body.data);

	} catch (e) {
		return sendResponse(res, PlaylistError.get("DATA_NOT_VALID_JSON"))
	}

	if(request.contentType && request.contentType === "live") return sendResponse(res, LivePlaylist.process(user, request.items));
	if(request.contentType && request.contentType === "vod") return sendResponse(res, VodPlaylist.process(user, request.items));

	sendResponse(res, PlaylistError.get("PARAM_CONTENT_TYPE_REQUIRED"))
};

/**
 *
 * @param res
 * @param data
 */
let sendResponse = function(res, data, err){
  
	//res.header('Access-Control-Allow-Origin' , '*' );

	if(typeof data !== "object") return res.status(500).send("Invalid response");

	if(typeof err !== "undefined") return res.status(500).json(data);
	res.json(data);
};
