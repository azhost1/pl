"use strict";
const fs = require('fs');

/**
 *
 * @param filePath
 * @returns {Settings}
 */
module.exports = function (filePath) {

	/**
	 *
	 */
	class Settings {

		/**
		 *
		 * @param filePath
		 */
		constructor(filePath) {

			if (!(typeof filePath === "string" || typeof filePath === "Buffer"))
				throw new Error("filePath must be an string or buffer");

			this.filePath = filePath;
			this.update();
		}

		/**
		 *
		 */
		update() {

			try {
				this.set(JSON.parse(fs.readFileSync(this.filePath)));
			} catch (err) {

				// File not exist... @TODO: Errors
				if (err.code === 'ENOENT') {
					this.config = null;
				} else {
					throw err;
				}
			}
		}

		/**
		 *
		 * @param config
		 */
		set(config) {
			config.lastConfigLoaded = Date.now();
			this.config = config;
		}

		/**
		 *
		 * @param seg
		 * @returns {*}
		 */
		get(seg) {
			if (typeof seg === "undefined") return this.config;

			if (this.config && this.config.hasOwnProperty(seg)) return this.config[seg];

			return null;
		}

	}


	return new Settings(filePath)
};
