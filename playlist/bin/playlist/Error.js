/**
 *
 * @param messages
 * @returns {PlaylistError}
 */
module.exports = function (messages) {


	/**
	 *
	 */
	class PlaylistError {

		/**
		 *
		 */
		constructor(){

			this.messages = {
				"PARAM_CONTENT_TYPE_REQUIRED": "Parametr contentType je povinný",
				"INVALID_SIGNATURE": "Neplatný podpis requestu",
				"REQUIRED_PARAM_MISSING": "Chybí některý z povinných parametrů",
				"UNKNOWN_CHANNEL": "Neznámý kanál",
				"UNKNOWN_PROTOCOL": "Neznámý playerType",
				"EXPIRED_KEY": "Klíč již expiroval",
				"ITEMS_MUST_BE_ARRAY": "Items musí být pole",
				"DATA_NOT_VALID_JSON": "Parametr data musí být validní JSON",
				"UNKNOWN_SOURCE": "Neznámý requestSource",
				"CZ_REGION_ONLY": "Video je dostupné pouze v ČR",
				"DRM_NOT_SUPPORTED": "Kombinace DRM a HLS není možná",
				"NOT_VALID_REQUEST": "Neplatný request, nepodařilo se zpracovat POST",
				"CAN_NOT_BE_PLAYED": "Video nemůže být přehráno",
				"EMPTY_PLAYLIST": "Prázdný playlist"
			};

		}

		/**
		 *
		 * @param code
		 * @returns {{CODE, RESULT}|*}
		 */
		get(code){
			if(this.messages.hasOwnProperty(code)) return this.formatResponse(code, this.messages[code]);

			return this.formatResponse("UNKNOWN_ERROR", "Neznámá chyba");
		}

		/**
		 *
		 * @param code
		 * @param message
		 * @returns {{CODE: string, RESULT: *}}
		 */
		formatResponse(code, message){
			return {
				CODE: "ERROR",
				RESULT: code
			}
		}

	}

	return new PlaylistError(messages)
};
