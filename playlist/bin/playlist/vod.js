let PlaylistError = require('./Error')();
let _ = require('underscore');
let crypto = require('crypto');

let cdnRequest = require('./cdnRequest');
let cdnToken = require('./cdnToken');


/**
 *
 * @param cdn
 * @returns {VodPlaylist}
 */
module.exports = function (cdn) {


	/**
	 *
	 */
	class VodPlaylist {

		/**
		 *
		 * @param cdn
		 */
		constructor(cdn) {
			this.cdn = cdn;
		}

		/**
		 {
			contentType: "vod",
			items: [
				{id: 19621854,key:"c4b56yfh2t4",canBePlay:true,drm:false,region:4,playerType:"dash",requestSource:"zoh",date: "2018-01-13 12:56:00"},
				{id: 19621855,key:"c4b5xfh2t4",canBePlay:false,drm:false,region:1,playerType:"dash",requestSource:"zoh",date: "2018-01-13 12:56:00"}
			]
		 }
		*/

		/**
		 *
		 * @param user
		 * @param items
		 * @returns {*}
		 */
		process(user, items) {

			if(typeof items !== 'object')
				return PlaylistError.get("ITEMS_MUST_BE_ARRAY");

			return this.parseItems(user, items);

		}

		/**
		 *
		 * @param user
		 * @param items
		 * @returns {*}
		 */
		parseItems(user, items){

			//Store for valid items
			let ids = [];

			for (let i = 0, len = items.length; i < len; i++) {
				let item = this.createParams(user.id, items[i]);

				item.playerType = item.playerType === "dash" ? "dash": "ios";

				item.drm = parseInt(item.drm);
				item.canBePlay = parseInt(item.canBePlay);

				//Check, if has all required params
				if(!this.allRequiredParams(item))
					return PlaylistError.get("REQUIRED_PARAM_MISSING");

				//Request from valid source?
				if(!this.cdn["CREDENTIALS"].hasOwnProperty(item["requestSource"]))
					return PlaylistError.get("UNKNOWN_SOURCE");

				//Is signed by key for source?
				if(!this.verifyKey(item))
					return PlaylistError.get("INVALID_SIGNATURE");

				//Expired sign date?
				if(+new Date(Date.parse(item.date)) < (+new Date()) - 3600000)
					return PlaylistError.get("EXPIRED_KEY");

				//Request to encoded format? (dash/hls)
				if(this.cdn["ALLOWED_PROTOCOLS"].indexOf(item.playerType.toLowerCase()) < 0)
					return PlaylistError.get("UNKNOWN_PROTOCOL");

				//Is readyForStreaming, not expired, etc...
				if (item.canBePlay != 1)
					return PlaylistError.get("CAN_NOT_BE_PLAYED");

				//Right, add to items for signing
				ids.push(this.getItemUrl(item));
			}

			if(ids.length < 1 ) return PlaylistError.get("EMPTY_PLAYLIST");

			return this.createResponse(ids);
		}

		/**
		 *
		 * @param userId
		 * @param request
		 * @returns {*}
		 */
		createParams(userId, request){

			let defaultParams = this.cdn["DEFAULT_PARAMS"];

			return _.extend(defaultParams, request, {
				expiry: Math.round(+new Date()/1000) + cdn["EXPIRY"],
				userId: userId,
				skipIpAddressCheck: false,
				id: request.id
			});

		}

		/**
		 *
		 * @param params
		 * @returns {boolean}
		 */
		allRequiredParams(params){
			let required = this.cdn["REQUIRED_PARAMS"]["VOD"];

			for (let i = 0, len = required.length; i < len; i++) {
				if(!params.hasOwnProperty(required[i])) return false;
			}

			return true;
		}

		/**
		 *
		 * @param req
		 * @returns {boolean}
		 */
		verifyKey(req){
			return this.signParams(req) == req.key;
		}

		/**
		 *
		 * @param params
		 */
		signParams(params){

			let str = (params.id + params.requestSource + params.date + params.region + params.canBePlay + params.drm).toString();

			return new Buffer(
				crypto.createHmac('SHA256', new Buffer(cdn["CREDENTIALS"][params.requestSource]))
					.update(str)
					.digest('hex')
			).toString('base64')
		}

		/**
		 *
		 * @param params
		 * @returns {string}
		 */
		vodResponse(params){
			let query = cdnRequest.create("vod", params);
			return this.cdn["URL"] + "?token=" + cdnToken.sign(query, this.cdn["SECRET"]) + "&" + query
		}

		/**
		 *
		 * @param item
		 * @returns {{streamUrls: {main: string}}}
		 */
		getItemUrl(item){
			return {
				streamUrls: {
					main: this.vodResponse(item),
				}
			}
		}

		/**
		 *
		 * @param ids
		 * @returns {{playlist: *}}
		 */
		createResponse(ids){

			return {
				CODE:  "OK",
				RESULT:  {
					playlist: ids
				}
			}
		}


	}

	return new VodPlaylist(cdn)
};