let cdnToken = module.exports = {};
let crypto = require("crypto");

/**
 *
 * @param query
 * @param secret
 */
cdnToken.sign = function(query, secret){
	return crypto.createHmac("sha1",
		new Buffer( secret, 'utf8')
	).update(query).digest("hex")
};
