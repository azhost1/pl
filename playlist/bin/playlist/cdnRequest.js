let _ = require('underscore');
let cdnRequest = module.exports = {};
let querystring = require('querystring');

/**
 *
 * @type {[*]}
 */
let allowedParams = [
	"contentType",
	"endOffset",
	"endTimestamp",
	"expiry",
	"id",
	"playerType",
	"quality",
	"region",
	"skipIpAddressCheck",
	"startOffset",
	"startTimestamp",
	"timeshiftWindow",
	"userId"
];

/**
 *
 * @param params
 * @returns {*}
 */
cdnRequest.create = function(type, params){
	params.contentType = type;
	return createQuery(sortedParams(params));
};

/**
 *
 * @param params
 * @returns {{}}
 */
let sortedParams = function(params){
	let r = {};

	_.map(allowedParams, function(param) {
		if(params.hasOwnProperty(param)) r[param] = params[param];
	});

	return r;

};


/**
 *
 * @param sortedParams
 * @returns {*}
 */
let createQuery = function(sortedParams){
	return querystring.stringify(sortedParams);
};