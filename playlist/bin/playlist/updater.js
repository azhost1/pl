let request = require('request');
let rp = require('request-promise');

module.exports = function (endPoint) {

	class Updater {

		/**
		 *
		 */
		constructor(endPoint) {
			this.endPoint = endPoint;
			this.data = null;
			this.loadData();
		}

		getData(seg){

			if (typeof seg === "undefined") return this.data;

			if (this.data && this.data.hasOwnProperty(seg)) return this.data[seg];
			return null;
		}

		setData(data){
			data.lastUpdate = Date.now();
			this.data = data;
		}

		loadData(){

			rp(this.endPoint)
				.then(data => this.setData(JSON.parse(data).result))
				.catch(function (err) {
					console.log(err, "err")
				});
		}

	}

	return new Updater(endPoint)
};