/**
 *
 * Created by PhpStorm, 30.1.18
 * @author Jiří Sakáč <Jiri.Sakac@ceskatelevize.cz>
 *
 */



module.exports = function (endPoint) {

	let Updater = require('./updater')(endPoint);

	/**
	 *
	 */
	class Variables {

		constructor() {
			this.updater = Updater;
			this.setupUpdater();
		}

		getData(seg){

			return this.updater.getData(seg);
		}

		setupUpdater(){
			setInterval(function(){
				Updater.loadData();
			}, 30000);
		}
	}

	return new Variables()
};