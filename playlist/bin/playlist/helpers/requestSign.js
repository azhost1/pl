let crypto = require('crypto');

/**
 *
 * @param secrets
 * @returns {RequestSign}
 */
module.exports = function (secrets) {

	/**
	 *
	 */
	class RequestSign {

		/**
		 *
		 * @param secrets
		 */
		constructor(secrets) {
			this.secrets = secrets;
		}

		/**
		 *
		 * @param projectId
		 * @param data
		 * @param key
		 * @returns {boolean}
		 */
		verify(projectId, data, key){
			return this.sign(projectId, data) === key;
		}

		/**
		 *
		 * @param projectId
		 * @param data
		 * @returns {string}
		 */
		sign(projectId, data){
			return new Buffer(
				crypto.createHmac('SHA256', this.secrets[projectId])
					.update(data)
					.digest('hex')
			).toString('base64')
		}

	}

	return new RequestSign(secrets)

};