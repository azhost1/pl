/**
 *
 * @param requiredParams
 * @returns {paramsValidator}
 */
module.exports = function (requiredParams) {

	/**
	 *
	 */
	class paramsValidator {

		/**
		 *
		 * @param requiredParams
		 */
		constructor(requiredParams) {
			this.requiredParams = requiredParams;
		}

		/**
		 *
		 * @param params
		 * @returns {boolean}
		 */
		validate(params){

			for (let i = 0, len = this.requiredParams.length; i < len; i++) {
				if(!params.hasOwnProperty(this.requiredParams[i])) {
				  return false;
				  
				}
			}

			return true;
		}


	}

	return new paramsValidator(requiredParams)

};