let PlaylistError = require('./Error')();
let _ = require('underscore');
let cluster = require('cluster');

let cdnRequest = require('./cdnRequest');
let cdnToken = require('./cdnToken');
let server, Variables, workers;

process.on('message', function(message) {
	//console.log(`Worker ${process.pid} recevies message '${JSON.stringify(message)}'`);
	//console.log(message.Variables)
	server = message.server;
	Variables = message.Variables.updater.data;
	workers = message.workers;
});

/**
 * @param cdn
 * @returns {LivePlaylist}
 */
module.exports = function (cdn) {


	/**
	 *
	 */
	class LivePlaylist {

		/**
		 *
		 * @param cdn
		 */
		constructor(cdn) {
			this.cdn = cdn;
			this.requestSignHelper = require( './helpers/requestSign' )(cdn["CREDENTIALS"]);
			this.paramsValidator = require( './helpers/paramsValidator' )(cdn["REQUIRED_PARAMS"]["LIVE"]);
		}

		/**
		 *
		 * @returns {*}
		 */
		getLastUpdate(){
			return Variables.lastUpdate;
		};

		/**
		 *
		 * @returns {*}
		 */
		health(){

			if(!Variables) return false;

			return {
				health: "OK",
				server: server,
				clusterWorkers: workers,
				instanceId: cluster.worker.id,
				lastUpdate: this.getLastUpdate()
			};
		}

		/**
		 *
		 * @param user
		 * @param items
		 * @returns {*}
		 */
		process(user, items){

			let item = items[0];

			if(typeof item !== 'object')
				return PlaylistError.get("ITEMS_MUST_BE_ARRAY");

			let params = this.createParams(user.id, item);
			if(!params.id) return PlaylistError.get("UNKNOWN_CHANNEL");

			if(!this.paramsValidator.validate(params))
				return PlaylistError.get("REQUIRED_PARAM_MISSING");

			if(!this.requestSignHelper.verify(params.requestSource, this.getdataForSign(params), params.key))
				return PlaylistError.get("INVALID_SIGNATURE");

			if(!this.isRequestFresh(params.date, cdn["EXPIRY"]))
				return PlaylistError.get("EXPIRED_KEY");

			if(cdn["ALLOWED_PROTOCOLS"].indexOf(params.playerType.toLowerCase()) < 0)
				return PlaylistError.get("UNKNOWN_PROTOCOL");

			let tvProgram = Variables["tvProgram"];

			if(tvProgram.hasOwnProperty(params.id)){
				params.region = tvProgram[params.id]["region"];
			}


			return this.liveResponse(params);
		}


		/**
		 *
		 * @param signDate
		 * @param expiry
		 * @returns {boolean}
		 */
		isRequestFresh(signDate, expiry){
			return +new Date(Date.parse(signDate)) > (+new Date()) - (expiry * 1000)
		}

		/**
		 *
		 * @param params
		 * @returns {*}
		 */
		getdataForSign(params){
			return params.id + params.requestSource + params.date;
		}

		/**
		 *
		 * @param userId
		 * @param request
		 * @returns {*}
		 */
		createParams(userId, request){

			let defaultParams = cdn["DEFAULT_PARAMS"];


			request.playerType = request.playerType === "hls" ? "ios" : "dash";

			//Default disable geoBlock for ČT24
			request.region = request.id == 2402 ? 1 : 4;

			return _.extend(defaultParams, request, {
				expiry: Math.round(+new Date()/1000) + cdn["EXPIRY"],
				userId: userId,
				skipIpAddressCheck: false,
				id: this.getChannelId(request.id)
			});

		}


		/**
		 *
		 * @param name
		 * @returns {*}
		 */
		getChannelId(name){
			let channel = _.find(Variables["encoders"], function(channel){
				return channel.encoderId.toLowerCase() == name
					|| channel.channelId.toLowerCase() == name;
			});

			if(channel && parseInt(channel.channelId) > 0) return parseInt(channel.channelId);

			return false;
		}

		/**
		 *
		 * @param params
		 * @returns {{CODE, RESULT}|*}
		 */
		liveResponse(params){

			delete params.key;

			let liveQuery = cdnRequest.create("live", params);
			let timeshiftQuery = cdnRequest.create("timeshift", params);

			return this.createResponse(
				params.id,
				cdn["URL"] + "?token=" + cdnToken.sign(liveQuery, cdn["SECRET"]) + "&" + liveQuery,
				cdn["URL"] + "?token=" + cdnToken.sign(timeshiftQuery, cdn["SECRET"]) + "&" + timeshiftQuery);
		}

		/**
		 *
		 * @param id
		 * @param live
		 * @param timeshift
		 * @returns {{CODE: string, RESULT: {playlist: [*]}}}
		 */
		createResponse(id, live, timeshift){

			return {
				CODE:  "OK",
				RESULT:  {
					playlist: [
						{
							id: id,
							streamUrls: {
								main: live,
								timeshift: timeshift
							}
						}
					]
				}
			}
		}


	}


	return new LivePlaylist(cdn)
};