let crypto = require("crypto");

/**
 *
 * @param bytes
 * @returns {User}
 */
module.exports = function (bytes) {

	/**
	 *
	 */
	class User {

		/**
		 *
		 * @param bytes
		 */
		constructor(bytes){
			this.bytes = bytes;
		}

		/**
		 *
		 * @returns {string}
		 */
		getUserId(){
			return crypto.randomBytes(this.bytes).toString("hex");
		}

		getUser(req){
			return {
				id: this.getUserId()
			}
		}

	}

	/**
	 * @returns User
	 */
	return new User(bytes)
};
