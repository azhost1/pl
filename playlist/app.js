"use strict";
const bodyParser = require("body-parser");
const express = require("express");
const path = require("path");
const indexRoute = require("./routes/index");

/**
 *
 */
class Server {

	/**
	 * Create Express app()
	 */
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

	/**
	 *
	 * @returns {Server}
	 */
	static bootstrap() {
        return new Server();
    }

	/**
	 * Configure webserver
	 */
	config() {

        //this.app.set("views", path.join(__dirname, "views"));
        //this.app.set("view engine", "jade");

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(express.static(path.join(__dirname, "public")));

        this.app.use(function (err, req, res, next) {
            var error = new Error("Not Found");
            err.status = 404;
            next(err);
        });

    }

	/**
	 * Bind routes
	 */
	routes() {
        let router;
        router = express.Router();
        let playlist = new indexRoute.Index();

        router.post("/", playlist.live.bind(playlist.live));
        router.get("/", playlist.hp.bind(playlist.hp));

        router.get("/crossdomain.xml", playlist.crossDomain.bind(playlist.crossDomain));


        this.app.use(router);
    }
}

/** @type {Server} */
let server = Server.bootstrap();

/** @type {express.Application} */
module.exports = server.app;
